from .models import Contenidos
from .models import Comentario
from django.http import HttpResponse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone

# Create your views here.

def anadir_o_reemplazar(llave, valor):
    try:
        yaexiste = Contenidos.objects.get(clave=llave)
        yaexiste.delete()
    except Contenidos.DoesNotExist:
        pass

    newContent = Contenidos(clave=llave, valor=valor)
    newContent.save()


@csrf_exempt
def index(request):
    if request.method == "POST":
        if request.POST['content'] and request.POST['value']:
            llave = request.POST['content']
            valor = request.POST['value']
            anadir_o_reemplazar(llave, valor)

    # Imprimir la plantilla
    almacenamiento_list = Contenidos.objects.all()
    template = loader.get_template('cms/index.html')
    context = { 'content_list': almacenamiento_list,
                'autenticado': request.user.is_authenticated}

    return HttpResponse(template.render(context, request))


@csrf_exempt
def get_content(request, nombre):
    if request.method == "PUT":
        value = request.body.decode('utf-8')
        anadir_o_reemplazar(nombre, value)

    if request.method == "POST":
        if request.POST['action'] == "Enviar Contenido":
            value = request.POST['value']
            anadir_o_reemplazar(nombre, value)

        elif request.POST['action'] == "Enviar Comentario":
            contenido = Contenidos.objects.get(clave=nombre)
            q = Comentario (contenido=contenido, cuerpo=request.POST['body'], fecha=timezone.now(), titulo=request.POST['title'])
            q.save()

        elif request.POST['action'] == "Actualizar Contenido":
            try:
                content = Contenidos.objects.get(clave=nombre)
                content.valor = request.POST['value']
                content.save()
            except Contenidos.DoesNotExist:
                pass

    # Preparo la respuesta HTML con las plantillas
    try:
        content = Contenidos.objects.get(clave=nombre)
        comentarios_list = content.comentario_set.all()

        template = loader.get_template('cms/show_content.html')
        context = {
            'content': content,
            'comentarios': comentarios_list,
            'autenticado': request.user.is_authenticated}

    except Contenidos.DoesNotExist:
        template = loader.get_template('cms/error.html')
        context = {'autenticado': request.user.is_authenticated}

    response = template.render(context, request)
    return HttpResponse(response)