from django.db import models

# Create your models here.


class Contenidos (models.Model):
    clave = models.TextField()
    valor = models.TextField()

    def __str__(self):
        return str(self.id) + ": " + self.clave + " --> " + self.valor


# Relacion 1 a n: un contenido puede tener n comentarios

class Comentario (models.Model):
    contenido = models.ForeignKey(Contenidos, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=128)
    cuerpo = models.TextField()
    fecha = models.DateTimeField()

    def __str__(self):
        return str(self.id) + ": " + self.titulo